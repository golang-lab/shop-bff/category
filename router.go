package main

import (
	"database/sql"
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	_ "github.com/go-sql-driver/mysql"
)

//#region entities

type Category struct {
	Id       string         `json:"id"`
	Name     string         `json:"name"`
	ParentId sql.NullString `json:"parentId"`
}

//#endregion entities

// default route, return success connection
func Endpoint(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{
		"result": "category server is running",
	})
}

// get category list
func CategoryGet(c *gin.Context) {
	db, err := sql.Open(config.Database.DB_Name, config.Database.Connection_String + "/shop_bff")
	if err != nil {
		panic(err)
	}

	var category Category
	list := make([]Category, 0)
	rows, err := db.Query("SELECT * FROM category")
	if err != nil {
		panic(err)
	}
	defer rows.Close()

	for rows.Next() {
		err := rows.Scan(&category.Id, &category.Name, &category.ParentId)
		if err != nil {
			panic(err)
		}
		list = append(list, category)
	}

	root := make([]*CategoryList, 0)
	leaf := make([]*CategoryList, 0)

	for _, val := range list {
		c := &CategoryList{
			parentId: val.ParentId,
			Id:       val.Id,
			Name:     val.Name,
			Children: make([]*CategoryList, 0)}

		if !val.ParentId.Valid {
			root = append(root, c)
		} else {
			leaf = append(leaf, c)
		}
	}

	for _, val := range root {
		dfs(val, leaf)
	}

	fmt.Println(root)

	c.JSON(http.StatusOK, gin.H{
		"result": root,
	})
}

type CategoryList struct {
	parentId sql.NullString

	Id       string          `json:"id"`
	Name     string          `json:"name"`
	Children []*CategoryList `json:"children"`
}

func dfs(root *CategoryList, list []*CategoryList) {
	for _, val := range list {
		if root.Id == val.parentId.String {
			root.Children = append(root.Children, val)
		}
	}
	for _, c := range root.Children {
		dfs(c, list)
	}
}
