package main

import (
	"fmt"
	"log"

	"github.com/gin-gonic/gin"

	// router "./router"

	viper "github.com/spf13/viper"
)

type Configuration struct {
	Port     int
	Database DBConfiguration
}

type DBConfiguration struct {
	DB_Name           string
	Connection_String string
}

var (
	config Configuration
)

func main() {

	viper.AddConfigPath("./config")
	viper.SetConfigType("yml")

	if err := viper.ReadInConfig(); err != nil {
		fmt.Printf("Error reading config file, %s", err)
	}
	if err := viper.Unmarshal(&config); err != nil {
		fmt.Printf("Error parsing config file, %s", err)
	}

	// Set up a http server.
	r := gin.Default()

	r.GET("/", Endpoint)

	r.GET("/category", CategoryGet)

	// Run http server
	if err := r.Run(fmt.Sprintf(":%d", config.Port)); err != nil {
		log.Fatalf("could not run server: %v", err)
	}
}
